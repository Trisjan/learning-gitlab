<?php


class Model
{
    protected static PDO $pdocon;

    public static function load()
    {
        if (isset(Model::$pdocon)=== false)
        {
            try {
                Model::$pdocon = new PDO("mysql:host=localhost;dbname=personal", "root", "Adsd123", [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
            } catch (PDOException $e) {
                die('Connection failed: ' . $e->getMessage());
            }
        }
    }
}