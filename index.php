<?php
session_start();

$router = new Router();
require 'core/Router.php';
$uri = trim(isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "/", '/');

$router->direct($uri);