<?php


class Router
{
    protected $routes = [];

    public function define($routes)
    {
        $this->routes = $routes;
    }

    public function direct($uri)
    {
        if (array_key_exists($uri, $this->routes)){
            $currentRoute = $this->routes [$uri];
            if(($currentRoute['authenticated'] && isset($_SESSION['user'])) || $currentRoute['authenticated'] === false){
                $controller = new $currentRoute['controller']();
                $controller->{$currentRoute['method']}();
                return;
            }
            else{
                header('Location: /login?error=Not%20Authenticated');
                die();
            }
        }
        throw new Exception("Deze route ken ik niet");
    }
}